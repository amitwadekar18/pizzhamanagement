package demo01;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Jdbc01Main {
	public static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver"; // MySQL >= 8.0
	public static final String DB_URL = "jdbc:mysql://localhost:3306/amit";
	public static final String DB_USER = "root";
	public static final String DB_PASSWORD = "manager";
	
	static {
		try {
			// load & register driver class
			Class.forName(DB_DRIVER);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public static void main(String[] args) {
		// create connection
				try(Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
					// prepare statement
					String sql = "SELECT id, name, author, subject, price FROM books";
					try(PreparedStatement stmt = con.prepareStatement(sql)) {
						// execute the statemet: stmt.executeQuery() or stmt.executeUpdate()
						try(ResultSet rs = stmt.executeQuery()) {
							// process result
							while(rs.next()) {
								int id = rs.getInt("id");
								String name = rs.getString("name");
								String author = rs.getString("author");
								String subject = rs.getString("subject");
								double price = rs.getDouble("price");
								System.out.printf("%d, %s, %s, %s, %f\n",
										id, name, author, subject, price);
							}
						}
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

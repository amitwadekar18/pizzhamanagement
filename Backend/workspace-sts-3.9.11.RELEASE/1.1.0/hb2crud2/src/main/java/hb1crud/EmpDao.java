package hb1crud;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

public class EmpDao {
	public Emp findById(int empno) {
		Emp e = HbUtil.getSession().find(Emp.class, empno);
		return e;
	}
	
	public List<Emp> findByDeptno(int deptno) {
		Criteria cr = HbUtil.getSession().createCriteria(Emp.class);
		cr.add(Restrictions.eq("deptno", deptno));
		return cr.list();
	}
	
	public Emp findByName(String ename) {
		Criteria cr = HbUtil.getSession().createCriteria(Emp.class);
		cr.add(Restrictions.eq("ename", ename));
		return (Emp) cr.uniqueResult();
	}

	public List<Emp> findByCriteria(DetachedCriteria dcr) {
		Session session = HbUtil.getSession();
		Criteria cr = dcr.getExecutableCriteria(session);
		return cr.list();
	}

}


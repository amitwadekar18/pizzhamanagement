package hb1crud;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class Hb2Main {
	public static void main(String[] args) {
		/*
		EmpDao dao = new EmpDao();
		try {
			HbUtil.beginTransaction();
			Emp e = dao.findById(7900);
			System.out.println("Found: " + e);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		EmpDao dao = new EmpDao();
		try {
			HbUtil.beginTransaction();
			List<Emp> list = dao.findByDeptno(10);
			list.forEach(System.out::println);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		EmpDao dao = new EmpDao();
		try {
			HbUtil.beginTransaction();
			DetachedCriteria dcr = DetachedCriteria.forClass(Emp.class);
			List<Emp> list = dao.findByCriteria(dcr);
			list.forEach(System.out::println);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/

		/*
		EmpDao dao = new EmpDao();
		try {
			HbUtil.beginTransaction();
			DetachedCriteria dcr = DetachedCriteria.forClass(Emp.class);
			dcr.add(Restrictions.eq("job", "ANALYST"));
			dcr.add(Restrictions.eq("deptno", 20));
			List<Emp> list = dao.findByCriteria(dcr);
			list.forEach(System.out::println);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		EmpDao dao = new EmpDao();
		try {
			HbUtil.beginTransaction();
			DetachedCriteria dcr = DetachedCriteria.forClass(Emp.class);			
			dcr.add(
				Restrictions.or(
					Restrictions.eq("job", "SALESMAN"),
					Restrictions.eq("deptno", 10)	
				)
			);
			dcr.addOrder(Order.desc("sal"));
			List<Emp> list = dao.findByCriteria(dcr);
			list.forEach(System.out::println);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		
		HbUtil.shutdown();
	}
}

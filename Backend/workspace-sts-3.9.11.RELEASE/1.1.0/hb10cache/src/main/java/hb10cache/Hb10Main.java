package hb10cache;

import org.hibernate.Session;

import hb10cache.HbUtil;

public class Hb10Main {
	public static void main(String[] args) {
		try {
			Session session1 = HbUtil.getSession();
			HbUtil.beginTransaction();
			Book b1 = session1.get(Book.class, 11); // data fetched from db
			System.out.println(b1);
			HbUtil.commitTransaction();
			Session session2 = HbUtil.getSession();
			HbUtil.beginTransaction();
			Book b2 = session2.get(Book.class, 11); // object returned from session cache
			System.out.println(b2);
			System.out.println("b1 == b2 : " + (b1 == b2));
			HbUtil.commitTransaction();
		}catch (Exception e) {
			HbUtil.rollbackTransaction();
			e.printStackTrace();
		}
		
		HbUtil.shutdown();
	}
}
package sh;

public class Hb03Main {
	public static void main(String[] args) {
		
		try(BookDaoImpl dao = new BookDaoImpl()) {
			dao.open();
			HbUtil.beginTransaction();
			Book b = dao.findById(11);
			System.out.println("Found " + b);
			HbUtil.commitTransaction();
		} // dao.close() 
		catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		
		try(BookDaoImpl dao = new BookDaoImpl()) {
			dao.open();
			HbUtil.beginTransaction();
			dao.addBook(new Book(61, "Yugandhar", "Shivaji Sawant", "Novell", 432.20));
			System.out.println("Book added.");
			HbUtil.commitTransaction();
		} // dao.close() 
		catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}

		try(BookDaoImpl dao = new BookDaoImpl()) {
			dao.open();
			HbUtil.beginTransaction();
			Book b = dao.findById(61);
			b.setPrice(534.10);
			dao.updateBook(b);
			System.out.println("Book updated.");
			HbUtil.commitTransaction();
		} // dao.close() 
		catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}

		try(BookDaoImpl dao = new BookDaoImpl()) {
			dao.open();
			HbUtil.beginTransaction();
			dao.deleteById(61);
			System.out.println("Book deleted.");
			HbUtil.commitTransaction();
		} // dao.close() 
		catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}

		HbUtil.shutdown();
	}
}
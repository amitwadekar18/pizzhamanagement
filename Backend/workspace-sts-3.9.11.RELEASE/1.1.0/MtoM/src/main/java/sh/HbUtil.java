package sh;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HbUtil {
	private final static SessionFactory factory = buildSessionFactory();
	private static StandardServiceRegistry registry;
	
	private static SessionFactory buildSessionFactory() {
		// hibernate 5 bootstrapping -- creating session factory.
		try {
			// create service registry
			registry = new StandardServiceRegistryBuilder()
					.applySetting("hibernate.format_sql", false)
					.configure() // read hibernate.cfg.xml (if available)
					.build();
			
			// create metadata object
			Metadata metadata = new MetadataSources(registry)
 					.getMetadataBuilder()
					.build();
					
			// create session factory
			return metadata.buildSessionFactory();
		} catch (HibernateException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}
	
	public static SessionFactory getSessionFactory() {
		return factory;
	}
	
	public static Session getCurrentSession() {
		return factory.getCurrentSession();
	}
	
	public static void beginTransaction() {
		Session session = factory.getCurrentSession();
		Transaction tx = session.getTransaction();
		tx.begin();
	}
	
	public static void commitTransaction() {
		Session session = factory.getCurrentSession();
		Transaction tx = session.getTransaction();
		tx.commit();
	}
	
	public static void rollbackTransaction() {
		Session session = factory.getCurrentSession();
		Transaction tx = session.getTransaction();
		tx.rollback();
	}
	
	public static void shutdown() {
		factory.close();
		registry.close();
	}

}
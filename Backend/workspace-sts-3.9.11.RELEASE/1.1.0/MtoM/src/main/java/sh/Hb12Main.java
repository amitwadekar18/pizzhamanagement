package sh;

import org.hibernate.Session;

public class Hb12Main {
	public static void main(String[] args) throws Exception {
		/*
		try(Session session = HbUtil.getCurrentSession()) {
			HbUtil.beginTransaction();
			Emp e = session.get(Emp.class, 7934);
			System.out.println(e);
			for (Meeting m : e.getMeetingList())
				System.out.println(m);
			HbUtil.commitTransaction();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/

		try(Session session = HbUtil.getCurrentSession()) {
			HbUtil.beginTransaction();
			Meeting m = session.get(Meeting.class, 2);
			System.out.println(m);
			for (Emp e : m.getEmpList())
				System.out.println(e);
			HbUtil.commitTransaction();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		
		HbUtil.shutdown();
	}
}

// CREATE TABLE empsys_idgen(entity CHAR(50) PRIMARY KEY, id INT);
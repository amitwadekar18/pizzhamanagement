package hb9;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="STUDENTS")
public class Student {
	@EmbeddedId
	private RollStd id;
	@Column
	private String name;
	@Column
	private double marks;

	public Student() {
	}

	public Student(RollStd id, String name, double marks) {
		this.id = id;
		this.name = name;
		this.marks = marks;
	}

	public RollStd getId() {
		return id;
	}

	public void setId(RollStd id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMarks() {
		return marks;
	}

	public void setMarks(double marks) {
		this.marks = marks;
	}

	@Override
	public String toString() {
		return String.format("Student [id=%s, name=%s, marks=%s]", id, name, marks);
	}
}

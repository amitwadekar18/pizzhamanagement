package hb9;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RollStd implements Serializable {
	@Column
	private int roll;
	@Column
	private int std;
	
	public RollStd() {
	}

	public RollStd(int roll, int std) {
		super();
		this.roll = roll;
		this.std = std;
	}

	public int getRoll() {
		return roll;
	}

	public void setRoll(int roll) {
		this.roll = roll;
	}

	public int getStd() {
		return std;
	}

	public void setStd(int std) {
		this.std = std;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(roll, std);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof RollStd))
			return false;
		RollStd other = (RollStd) obj;
		return roll == other.roll && std == other.std;
	}

	@Override
	public String toString() {
		return String.format("RollStd [roll=%s, std=%s]", roll, std);
	}
}

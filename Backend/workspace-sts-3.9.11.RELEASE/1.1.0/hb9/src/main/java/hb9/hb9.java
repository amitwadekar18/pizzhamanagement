package hb9;

import org.hibernate.Session;

public class hb9 {
	public static void main(String[] args) {
		try {
			Session session = HbUtil.getSession();
			HbUtil.beginTransaction();
			Student s = session.get(Student.class, new RollStd(2, 1));
			System.out.println(s);
			HbUtil.commitTransaction();
		}catch (Exception e) {
			HbUtil.rollbackTransaction();
			e.printStackTrace();
		}
		
		HbUtil.shutdown();
	}
}
package com.amitwadekar.services;

import java.util.List;

import com.amitwadekar.entities.Order;

public interface OrderService {
	void addOrderWithDetailsService(Order o);
	List<Order> getAllOrderswithDetailsService();
	List<Order> getOrderOnStatus(String status);
	Order getOrderByID(int orderID);
}

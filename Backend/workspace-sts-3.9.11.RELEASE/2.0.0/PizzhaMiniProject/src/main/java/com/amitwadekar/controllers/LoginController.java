package com.amitwadekar.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.amitwadekar.entities.Customer;
import com.amitwadekar.models.ResponseModel;
import com.amitwadekar.services.LoginService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@RestController
public class LoginController {

	@Autowired
	private LoginService loginService;
	
	private ResponseModel respModel;
	
	ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	
	@CrossOrigin
	@PostMapping(value="/newCustomer", headers="Accept=application/json")
	public String addItem(@RequestBody Customer c)throws JsonProcessingException{
		try {
			loginService.addNewCustomer(c);
			respModel = new ResponseModel("true",null);
			return ow.writeValueAsString(respModel);
		}catch(Exception e){
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	@CrossOrigin
	@PostMapping(value="/getCustomerByMail", headers="Accept=application/json")
	public String getCustomerByMail(@RequestBody Customer c)throws JsonProcessingException{
		try {
			Customer cust = loginService.findByEmail(c.getCemail());
			respModel = new ResponseModel("true",cust);
			return ow.writeValueAsString(respModel);
		}catch(Exception e){
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	@CrossOrigin
	@PostMapping(value="/login", headers="Accept=application/json")
	public String login(@RequestBody Customer c)throws JsonProcessingException{
		try {
			Customer cust = loginService.findByEmailPassword(c.getCemail(), c.getCpassword());
			respModel = new ResponseModel("true",cust);
			return ow.writeValueAsString(respModel);
		}catch(Exception e){
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	@CrossOrigin
	@PostMapping(value="/resetPwd", headers="Accept=application/json")
	public String resetPassword(@RequestBody Customer c)throws JsonProcessingException{
		try {
			//sCustomer cust = loginService.findByEmail(c.getCemail());
			int result = loginService.resetPassword(c.getCemail(),c.getCpassword());
			respModel = new ResponseModel("true",result);
			return ow.writeValueAsString(respModel);
		}catch(Exception e){
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
}

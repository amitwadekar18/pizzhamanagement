package com.amitwadekar.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amitwadekar.daos.MenuDao;
import com.amitwadekar.entities.Item;
import com.amitwadekar.entities.ItemPrice;

@Service
public class MenuServiceImpl implements MenuService {

	@Autowired
	private MenuDao dao;

	@Transactional
	@Override
	public List<Item> getItems() {
		System.out.println("inside service");
		List<Item> list = dao.getItemList();
		for(Item i: list) {
			List<ItemPrice> ip = i.getiItemPrice();
			ip.toString();
		}
		return list;
	}

	@Transactional
	@Override
	public void insert(Item i) {
		dao.add(i);
	}

	@Transactional
	@Override
	public List<String> getTypes() {
		System.out.println("inside service");
		List<String> list = dao.getTypeList();
		return list;
	}
	
	@Transactional
	@Override
	public List<String> getSubTypesService(String type) {
		System.out.println("inside service");
		List<String> list = dao.getSubTypes(type);
		return list;
	}

	@Transactional
	@Override
	public List<Item> getPizzaByGivenTypeandSubTypeService(String category, String type) {
		System.out.println("inside service");
		List<Item> list = dao.getPizzaByGivenTypeandSubType(category,type);
		return list;
	}

	@Transactional
	@Override
	public Item getPizzabyIdService(int id) {
		System.out.println("inside service");
		Item i = dao.getPizzabyId(id);
		return i;
	}

	@Transactional
	@Override
	public List<ItemPrice> getPriceByIdService(int id) {
		System.out.println("inside service");
		Item i = dao.getPizzabyId(id);
		List<ItemPrice> price = i.getiItemPrice();
		return price;
	}

}

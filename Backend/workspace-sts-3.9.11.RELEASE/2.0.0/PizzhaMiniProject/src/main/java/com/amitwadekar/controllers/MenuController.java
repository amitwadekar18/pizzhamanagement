package com.amitwadekar.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.amitwadekar.entities.Item;
import com.amitwadekar.models.ResponseModel;
import com.amitwadekar.services.MenuService;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@RestController
public class MenuController {

	@Autowired
	private MenuService menuService;
	
	private ResponseModel respModel;
	
	ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	
	@CrossOrigin("http://localhost:4200")
	@GetMapping("/itemmenu")
	public String getMenu() throws JsonProcessingException {
		System.out.println("In MenuController");
		try {
			List<Item> lst = menuService.getItems();
			return ow.writeValueAsString(new ResponseModel("true",lst));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	@CrossOrigin
	@PostMapping(value="/addMenuItem", headers="Accept=application/json")
	public String addItem(@RequestBody Item i)throws JsonProcessingException{
		try {
			menuService.insert(i);
			respModel = new ResponseModel("true",null);
			return ow.writeValueAsString(respModel);
		}catch(Exception e){
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	@CrossOrigin
	@GetMapping("/itemtype")
	public String getMenuType() throws JsonProcessingException {
		System.out.println("In MenuController");
		try {
			List<String> lst = menuService.getTypes();
			return ow.writeValueAsString(new ResponseModel("true",lst));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	@CrossOrigin
	@PostMapping(value="/itemsubtype", headers="Accept=application/json")
	public String getMenuSubType(@RequestBody Item i)throws JsonProcessingException{
		System.out.println("In MenuController");
		try {
			System.out.println("the strng is : "+i.getItype());
			List<String> lst = menuService.getSubTypesService(i.getItype());
			return ow.writeValueAsString(new ResponseModel("true",lst));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	@CrossOrigin
	@PostMapping(value="/getpizza", headers="Accept=application/json")
	@JsonIgnore
	public String getpizzabycategorytype(@RequestBody Item i)throws JsonProcessingException{
		System.out.println("In MenuController");
		try {
			System.out.println("the type is : "+i.getItype());
			System.out.println("the cat is : "+i.getIcategory());
			List<Item> lst = menuService.getPizzaByGivenTypeandSubTypeService(i.getIcategory(),i.getItype());
			return ow.writeValueAsString(new ResponseModel("true",lst));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	@CrossOrigin
	@PostMapping(value="/pizzabyid", headers="Accept=application/json")
	public String getpizzabyid(@RequestBody Item i)throws JsonProcessingException{
		System.out.println("In MenuController");
		try {
			System.out.println("the strng is : "+i.getiID());
			Item lst = menuService.getPizzabyIdService(i.getiID());
			return ow.writeValueAsString(new ResponseModel("true",lst));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	@CrossOrigin
	@PostMapping(value="/pricebyid", headers="Accept=application/json")
	public String getpricebyid(@RequestBody Item i)throws JsonProcessingException{
		System.out.println("In MenuController");
		try {
			System.out.println("the strng is : "+i.getiID());
			Object lst = menuService.getPriceByIdService(i.getiID());
			return ow.writeValueAsString(new ResponseModel("true",lst));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
}

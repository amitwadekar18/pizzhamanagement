package com.amitwadekar.models;

public class ResponseModel {
	private String status;
	private Object body;
	
	public ResponseModel() {
		this("","");
	}
	
	public ResponseModel(String status, Object body) {
		this.status = status;
		this.body = body;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getBody() {
		return body;
	}

	public void setBody(Object body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "ResponseModel [status=" + status + ", body=" + body + "]";
	}
	
}

package com.amitwadekar.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "PIZZA_ITEMS")
public class Item implements Serializable {
	@Id
	@Column(name = "ID")
	private int iID;
	@Column(name = "Name")
	private String iname;
	@Column(name = "Type")
	private String itype;
	@Column(name = "Category")
	private String icategory;
	@Column(name = "Description")
	private String idescription;
	
	@OneToMany(mappedBy = "ipid", fetch = FetchType.EAGER)
	private List<ItemPrice> iItemPrice;
	
	public Item() {
		this(0,"","","","");
		this.iItemPrice = new ArrayList<ItemPrice>();
	}
	
	public Item(int iID, String iname, String itype, String icategory, String idescription) {
		this.iID = iID;
		this.iname = iname;
		this.itype = itype;
		this.icategory = icategory;
		this.idescription = idescription;
		this.iItemPrice = new ArrayList<ItemPrice>();
	}



	public int getiID() {
		return iID;
	}
	public void setiID(int iID) {
		this.iID = iID;
	}
	public String getIname() {
		return iname;
	}
	public void setIname(String iname) {
		this.iname = iname;
	}
	public String getItype() {
		return itype;
	}
	public void setItype(String itype) {
		this.itype = itype;
	}
	public String getIcategory() {
		return icategory;
	}
	public void setIcategory(String icategory) {
		this.icategory = icategory;
	}
	public String getIdescription() {
		return idescription;
	}
	public void setIdescription(String idescription) {
		this.idescription = idescription;
	}

	public List<ItemPrice> getiItemPrice() {
		return iItemPrice;
	}

	public void setiItemPrice(List<ItemPrice> iItemPrice) {
		this.iItemPrice = iItemPrice;
	}

	@Override
	public String toString() {
		return "Item [iID=" + iID + ", iname=" + iname + ", itype=" + itype + ", icategory=" + icategory
				+ ", idescription=" + idescription + "]";
	}
	
	
	
}

package com.amitwadekar.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "PIZZA_ORDERS")
public class Order implements Serializable {
	@Id
	@Column(name = "ID")
	private int oid;
	@Column(name = "CustomerId")
	private int ocustomerid;
	@Column(name = "OrderTime")
	private Date oordertime;
	@Column(name = "Status")
	private String ostatus;
	
	@OneToMany(mappedBy = "odOrderId", fetch = FetchType.LAZY)
	private List<OrderDetails> oOD;
	
	public Order() {
		this(0,0,null,"");
		this.oOD = new ArrayList<OrderDetails>();
	}
	
	public Order(int oid, int ocustomerid, Date oordertime, String ostatus) {
		this.oid = oid;
		this.ocustomerid = ocustomerid;
		this.oordertime = oordertime;
		this.ostatus = ostatus;
		this.oOD = new ArrayList<OrderDetails>();
	}

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public int getOcustomerid() {
		return ocustomerid;
	}

	public void setOcustomerid(int ocustomerid) {
		this.ocustomerid = ocustomerid;
	}

	public Date getOordertime() {
		return oordertime;
	}

	public void setOordertime(Date oordertime) {
		this.oordertime = oordertime;
	}

	public String getOstatus() {
		return ostatus;
	}

	public void setOstatus(String ostatus) {
		this.ostatus = ostatus;
	}
	
	public List<OrderDetails> getoOD() {
		return oOD;
	}

	public void setoOD(List<OrderDetails> oOD) {
		this.oOD = oOD;
	}

	@Override
	public String toString() {
		return "Order [oid=" + oid + ", ocustomerid=" + ocustomerid + ", oordertime=" + oordertime + ", ostatus="
				+ ostatus + ", oOD=" + oOD + "]";
	}

	
	
	
}

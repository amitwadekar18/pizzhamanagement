package com.amitwadekar.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amitwadekar.daos.OrderDao;
import com.amitwadekar.entities.ItemPrice;
import com.amitwadekar.entities.Order;
import com.amitwadekar.entities.OrderDetails;

@Service
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	OrderDao dao;

	@Transactional
	@Override
	public void addOrderWithDetailsService(Order o) {
		System.out.println("inside service");
		dao.addOrderwithDetail(o);
	}
	
	@Transactional
	@Override
	public List<Order> getAllOrderswithDetailsService(){
		System.out.println("inside service");
		List<Order> lst = dao.getAllOrderswithDetails();
		for (Order o : lst) {
			List<OrderDetails> od = o.getoOD();
			for (OrderDetails odTest : od) {
				List<ItemPrice> ip = odTest.getItemPrice();
				ip.toString();
			}
			System.out.println(od);
		}
		return lst;
	}
	
	@Transactional
	@Override
	public List<Order> getOrderOnStatus(String status){
		System.out.println("inside service");
		List<Order> lst = dao.getOrderOnStatus(status);
		for (Order o : lst) {
			List<OrderDetails> od = o.getoOD();
			for (OrderDetails odTest : od) {
				List<ItemPrice> ip = odTest.getItemPrice();
				ip.toString();
			}
			System.out.println(od);
		}
		return lst;
	}

	@Transactional
	@Override
	public Order getOrderByID(int orderID) {
		System.out.println("inside service");
		Order o = dao.getOrderByID(orderID);
		List<OrderDetails> od = o.getoOD();
		for (OrderDetails odTest : od) {
			List<ItemPrice> ip = odTest.getItemPrice();
			ip.toString();
		}
		return o;
	}
	
	
}

package com.amitwadekar.daos;

import java.util.List;

import com.amitwadekar.entities.Item;

public interface MenuDao {
	void add(Item i);
	List<Item> getItemList();
	List<String> getTypeList();
	List<String> getSubTypes(String type);
	List<Item> getPizzaByGivenTypeandSubType(String category,String type);
	Item getPizzabyId(int id);
	Item getPriceById(int id);
}

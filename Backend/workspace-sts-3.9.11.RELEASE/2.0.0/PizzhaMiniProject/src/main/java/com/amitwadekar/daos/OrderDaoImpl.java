package com.amitwadekar.daos;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amitwadekar.entities.Order;

@Repository
public class OrderDaoImpl implements OrderDao {

	@Autowired
	private SessionFactory factory;

	@Override
	public void addOrderwithDetail(Order o) {
		System.out.println("inside dao");
		Session session = factory.getCurrentSession();
		session.persist(o);
	}

	@Override
	public List<Order> getAllOrderswithDetails() {
		System.out.println("inside dao");
		Session session = factory.getCurrentSession();
		Query q = session.createQuery("from Order");
		return q.getResultList();
	}

	@Override
	public List<Order> getOrderOnStatus(String status) {
		String hql = "from Order o where o.ostatus = :s_Status";
		Session session = factory.getCurrentSession();
		Query q = session.createQuery(hql);
		q.setParameter("s_Status", status);
		return q.getResultList();
	}

	@Override
	public Order getOrderByID(int orderID) {
		String hql = "from Order o where o.oid = :s_ID";
		Session session = factory.getCurrentSession();
		Query q = session.createQuery(hql);
		q.setParameter("s_ID", orderID);
		return (Order)q.getSingleResult();
	}
	
	
}

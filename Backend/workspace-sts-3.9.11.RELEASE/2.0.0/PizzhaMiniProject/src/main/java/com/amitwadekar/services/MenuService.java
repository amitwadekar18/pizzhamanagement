package com.amitwadekar.services;

import java.util.List;

import com.amitwadekar.entities.Item;
import com.amitwadekar.entities.ItemPrice;

public interface MenuService {
	List<Item> getItems();
	void insert(Item i);
	List<String> getTypes();
	List<String> getSubTypesService(String type);
	List<Item> getPizzaByGivenTypeandSubTypeService(String category,String type);
	Item getPizzabyIdService(int id);
	List<ItemPrice> getPriceByIdService(int id);
}

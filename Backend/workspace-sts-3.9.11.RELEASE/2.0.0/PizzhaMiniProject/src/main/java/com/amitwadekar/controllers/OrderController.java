package com.amitwadekar.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.amitwadekar.entities.Item;
import com.amitwadekar.entities.Order;
import com.amitwadekar.entities.OrderDetails;
import com.amitwadekar.models.ResponseModel;
import com.amitwadekar.services.OrderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@RestController
public class OrderController {
	
	@Autowired
	OrderService orderService;
	
	private ResponseModel respModel;
	
	ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	
	@CrossOrigin
	@PostMapping(value="/addOrder", headers="Accept=application/json")
	public String addOrder(@RequestBody Order o)throws JsonProcessingException{
		System.out.println("inside controller");
		try {
				System.out.println("oder is : " + o);
				orderService.addOrderWithDetailsService(o);
				respModel = new ResponseModel("true",null);
				return ow.writeValueAsString(respModel);
			}catch(Exception e){
				e.printStackTrace();
			}
			return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	@CrossOrigin
	@GetMapping("/allOrders")
	public String getAllOrderwithDetails()throws JsonProcessingException{
		System.out.println("inside controller");
		try {
			List<Order> lst = orderService.getAllOrderswithDetailsService();

			respModel = new ResponseModel("true",lst);
			return ow.writeValueAsString(respModel);
		}catch(Exception e){
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	@CrossOrigin
	@PostMapping("/getOrderonStatus")
	public String getAllOrderOnDetails(@RequestBody Order o)throws JsonProcessingException{
		System.out.println("inside controller");
		try {
			List<Order> lst = orderService.getOrderOnStatus(o.getOstatus());
			respModel = new ResponseModel("true",lst);
			return ow.writeValueAsString(respModel);
		}catch(Exception e){
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	@CrossOrigin
	@PostMapping("/getOrderById")
	public String getAllOrderById(@RequestBody Order o)throws JsonProcessingException{
		System.out.println("inside controller");
		try {
			Order ordr = orderService.getOrderByID(o.getOid());
			respModel = new ResponseModel("true",ordr);
			return ow.writeValueAsString(respModel);
		}catch(Exception e){
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}

}

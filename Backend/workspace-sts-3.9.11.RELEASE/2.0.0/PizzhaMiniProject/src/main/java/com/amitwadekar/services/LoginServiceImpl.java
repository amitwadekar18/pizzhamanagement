package com.amitwadekar.services;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.amitwadekar.daos.LoginDao;
import com.amitwadekar.entities.Customer;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private LoginDao dao;

	@Transactional
	@Override
	public void addNewCustomer(Customer c) {
		System.out.println("inside service");
		dao.addNewCustomer(c);
		
	}
	
	@Transactional
	@Override
	public Customer findByEmail(String email) {
		System.out.println("inside service");
		Customer c = dao.findByEmail(email);
		return c;
	}
	
	@Transactional
	@Override
	public Customer findByEmailPassword(String email, String password) {
		System.out.println("inside service");
		Customer c = dao.findByEmailPassword(email, password);
		return c;
	}
	
	@Transactional
	@Override
	public int resetPassword(String email, String password) {
		System.out.println("inside service");
		return dao.resetPassword(email, password);
	}

}

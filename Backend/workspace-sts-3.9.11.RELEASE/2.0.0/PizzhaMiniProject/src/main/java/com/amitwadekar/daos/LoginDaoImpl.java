package com.amitwadekar.daos;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amitwadekar.entities.Customer;

@Repository
public class LoginDaoImpl implements LoginDao {

	@Autowired
	private SessionFactory factory;
	
	@Override
	public void addNewCustomer(Customer c) {
		Session session = factory.getCurrentSession();
		session.persist(c);
	}

	@Override
	public Customer findByEmail(String email) {
		String hql = "from Customer c where c.cemail = :s_Email";
		Session session = factory.getCurrentSession();
		Query q = session.createQuery(hql);
		q.setParameter("s_Email", email);
		return (Customer)q.getSingleResult();
	}

	@Override
	public Customer findByEmailPassword(String email, String password) {
		String hql = "from Customer c where c.cemail = :s_Email and c.cpassword = :s_Password";
		Session session = factory.getCurrentSession();
		Query q = session.createQuery(hql);
		q.setParameter("s_Email", email);
		q.setParameter("s_Password", password);
		return (Customer)q.getSingleResult();
	}
	
	@Override
	public int resetPassword(String email, String password) {
		String hql = "update Customer c set c.cpassword= :s_Password where c.cemail = :s_Email";
		Session session = factory.getCurrentSession();
		Query q = session.createQuery(hql);
		q.setParameter("s_Password", password);
		q.setParameter("s_Email", email);
		return q.executeUpdate();
	}

}

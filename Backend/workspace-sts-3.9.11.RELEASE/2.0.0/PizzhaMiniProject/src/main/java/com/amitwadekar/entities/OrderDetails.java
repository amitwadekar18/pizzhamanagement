package com.amitwadekar.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "PIZZA_ORDERDETAILS")
public class OrderDetails implements Serializable{
	@Id
	@Column(name = "ID")
	private int odid;
	@Column(name = "OrderId")
	private int odOrderId;
	@Column(name = "PRICEID")
	private int odpriceid;
	
	@OneToMany(mappedBy = "ipid", fetch = FetchType.LAZY)
	private List<ItemPrice> itemPrice;
	
	public OrderDetails() {
		this(0,0,0);
		this.itemPrice = new ArrayList<ItemPrice>();
		
	}
	
	public OrderDetails(int odid, int odOrderId, int odpriceid) {
		this.odid = odid;
		this.odOrderId = odOrderId;
		this.odpriceid = odpriceid;
		this.itemPrice = new ArrayList<ItemPrice>();
	}
	public int getOdid() {
		return odid;
	}
	public void setOdid(int odid) {
		this.odid = odid;
	}
	public int getOdOrderId() {
		return odOrderId;
	}
	public void setOdOrderId(int odOrderId) {
		this.odOrderId = odOrderId;
	}
	public int getOdpriceid() {
		return odpriceid;
	}
	public void setOdpriceid(int odpriceid) {
		this.odpriceid = odpriceid;
	}
	
	public List<ItemPrice> getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(List<ItemPrice> itemPrice) {
		this.itemPrice = itemPrice;
	}

	@Override
	public String toString() {
		return "OrderDetails [odid=" + odid + ", odOrderId=" + odOrderId + ", odpriceid=" + odpriceid + "]";
	}
	
	
	
}

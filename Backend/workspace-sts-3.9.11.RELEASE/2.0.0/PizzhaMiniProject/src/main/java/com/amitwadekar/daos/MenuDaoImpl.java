package com.amitwadekar.daos;

import java.util.List;

import javax.ejb.SessionSynchronization;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amitwadekar.entities.Item;

@Repository
public class MenuDaoImpl implements MenuDao {

	@Autowired
	private SessionFactory factory;
	
	@Override
	public void add(Item i) {
		Session session = factory.getCurrentSession();
		session.persist(i);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Item> getItemList() {
		Session session = factory.getCurrentSession();
		Query q = session.createQuery("from Item");
		return (List<Item>)q.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getTypeList() {
		Session session = factory.getCurrentSession();
		String hql = "select Type from PIZZA_ITEMS group by Type";
		NativeQuery<String> q = session.createSQLQuery(hql);
		return q.getResultList();
	}

	@Override
	public List<String> getSubTypes(String type) {
		String hql = "select i.icategory from Item i where i.itype = :s_Type group by i.icategory";
		Session session = factory.getCurrentSession();
		Query q = session.createQuery(hql);
		q.setParameter("s_Type", type);
		return q.getResultList();
	}

	@Override
	public List<Item> getPizzaByGivenTypeandSubType(String category, String type) {
		String hql = "from Item i where i.itype = :s_Type and i.icategory = :s_Cateogry";
		Session session = factory.getCurrentSession();
		Query q = session.createQuery(hql);
		q.setParameter("s_Cateogry", category);
		q.setParameter("s_Type", type);
		return q.getResultList();
	}

	@Override
	public Item getPizzabyId(int id) {
		String hql = "from Item i where i.iID = :s_ID";
		Session session = factory.getCurrentSession();
		Query q = session.createQuery(hql);
		q.setParameter("s_ID", id);
		return (Item)q.getSingleResult();
	}

	@Override
	public Item getPriceById(int id) {
		String hql = "from Item i where i.iID = :s_ID";
		Session session = factory.getCurrentSession();
		Query q = session.createQuery(hql);
		q.setParameter("s_ID", id);
		return (Item)q.getSingleResult();
	}
}

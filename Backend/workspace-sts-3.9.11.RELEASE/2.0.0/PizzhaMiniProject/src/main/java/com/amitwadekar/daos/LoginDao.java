package com.amitwadekar.daos;

import com.amitwadekar.entities.Customer;

public interface LoginDao {
	void addNewCustomer(Customer c);
	Customer findByEmail(String email);
	Customer findByEmailPassword(String email,String password);
	int resetPassword(String email,String password);
}

package com.amitwadekar.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name= "PIZZA_CUSTOMERS")
public class Customer implements Serializable {
	
	@Id
	@Column(name = "ID")
	private int cid;
	@Column(name = "Name")
	private String cname;
	@Column(name = "Password")
	private String cpassword;
	@Column(name = "Mobile")
	private String cmobile;
	@Column(name = "Address")
	private String caddr;
	@Column(name = "Email")
	private String cemail;
	
	@Transient
	@OneToMany(mappedBy = "oid", fetch = FetchType.LAZY)
	private List<Order> cOrderList;
	public Customer() {
		this(0,"","","","","");
		this.cOrderList = new ArrayList<Order>();
	}
	
	public Customer(int cid, String cname, String cpassword, String cmobile, String caddr, String cemail) {
		this.cid = cid;
		this.cname = cname;
		this.cpassword = cpassword;
		this.cmobile = cmobile;
		this.caddr = caddr;
		this.cemail = cemail;
		this.cOrderList = new ArrayList<Order>();
	}
	
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getCpassword() {
		return cpassword;
	}
	public void setCpassword(String cpassword) {
		this.cpassword = cpassword;
	}
	public String getCmobile() {
		return cmobile;
	}
	public void setCmobile(String cmobile) {
		this.cmobile = cmobile;
	}
	public String getCaddr() {
		return caddr;
	}
	public void setCaddr(String caddr) {
		this.caddr = caddr;
	}
	public String getCemail() {
		return cemail;
	}
	public void setCemail(String cemail) {
		this.cemail = cemail;
	}

	@Override
	public String toString() {
		return "Customer [cid=" + cid + ", cname=" + cname + ", cpassword=" + cpassword + ", cmobile=" + cmobile
				+ ", caddr=" + caddr + ", cemail=" + cemail + "]";
	}
	
	
	
}

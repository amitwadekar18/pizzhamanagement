package com.amitwadekar.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PIZZA_PRICING")
public class ItemPrice implements Serializable {
	@Id
	@Column(name = "ID")
	private int ipid;
	@Column(name = "ITEMID")
	private int ipitemid;
	@Column(name = "SIZES")
	private String ipsize;
	@Column(name = "Price")
	private Double ipprice;
	
	public ItemPrice() {
		this(0,0,"",0.0);
	}

	public ItemPrice(int ipid, int ipitemid, String ipsize, Double ipprice) {
		this.ipid = ipid;
		this.ipitemid = ipitemid;
		this.ipsize = ipsize;
		this.ipprice = ipprice;
	}

	public int getIpid() {
		return ipid;
	}

	public void setIpid(int ipid) {
		this.ipid = ipid;
	}

	public int getIpitemid() {
		return ipitemid;
	}

	public void setIpitemid(int ipitemid) {
		this.ipitemid = ipitemid;
	}

	public String getIpsize() {
		return ipsize;
	}

	public void setIpsize(String ipsize) {
		this.ipsize = ipsize;
	}
	

	public Double getIpprice() {
		return ipprice;
	}

	public void setIpprice(Double ipprice) {
		this.ipprice = ipprice;
	}

	@Override
	public String toString() {
		return "ItemPrice [ipid=" + ipid + ", ipitemid=" + ipitemid + ", ipsize=" + ipsize + ", ipprice=" + ipprice
				+ "]";
	}

	
	
	
}

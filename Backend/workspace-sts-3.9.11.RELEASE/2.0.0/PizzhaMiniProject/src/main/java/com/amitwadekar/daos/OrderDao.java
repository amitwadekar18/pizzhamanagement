package com.amitwadekar.daos;

import java.util.List;

import com.amitwadekar.entities.Order;

public interface OrderDao {
	void addOrderwithDetail(Order o);
	List<Order> getAllOrderswithDetails();
	List<Order> getOrderOnStatus(String status);
	Order getOrderByID(int orderID);
}

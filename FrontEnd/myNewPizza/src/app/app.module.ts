import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {HttpClientModule } from '@angular/common/http';
import { ItemserviceService } from './services/itemservice.service';
import { PizzaListComponentComponent } from './component/pizza-list-component/pizza-list-component.component';
import { PizzaCategoryComponentComponent } from './component/pizza-category-component/pizza-category-component.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RegisterComponent } from './user/register/register.component';
import { LoginComponent } from './user/login/login.component';
import { ForgotPasswordComponent } from './user/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './user/reset-password/reset-password.component';
import { CartComponent } from './component/cart/cart.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PizzaListComponentComponent,
    PizzaCategoryComponentComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {path: 'home', component: HomeComponent},
      {path: 'login', component: LoginComponent },  
      {path: 'register', component: RegisterComponent }, 
      {path: 'frgpwd', component: ForgotPasswordComponent },  
      {path: 'cart', component: CartComponent },  
      {path: 'reset/:c_email', component: ResetPasswordComponent },
      {path: 'listAll', component: PizzaListComponentComponent },
      {path: '**', component: LoginComponent }
    ])
  ],
  providers: [ItemserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }

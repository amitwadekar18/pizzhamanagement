import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Resp } from 'src/app/entites/resp';
import { LoginServiceService } from 'src/app/service/login-service.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  r_email: any;
  r_password: any;
  r_c_password: any;
  res: Resp | undefined;

  constructor(private lservice: LoginServiceService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    
  }

  resetPassword(){
    this.route.params.subscribe((params: Params) => this.r_email = params['c_email']);
    this.lservice.resetPassword(this.r_email, this.r_password).subscribe(
      data =>{
        this.res = data;
        if (this.res.status == "true"){
          console.log("Password Has been reset");
          this.router.navigate(["login"]);
        }else{
          console.log("soemthing went wrong");
        }
      }
    )

  }

}

import { Component, OnInit } from '@angular/core';
import { inject } from '@angular/core/testing';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Resp } from 'src/app/entites/resp';
import { LoginServiceService } from '../../service/login-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  constructor(private lservice: LoginServiceService,
    private router: Router) { }

  c_email: any;
  c_password: any;
  res: Resp | undefined;
  

  ngOnInit(): void {
  }

  public register() {
    this.router.navigate(['register']);
  }

  loginCheck() {
    this.lservice.loginMethod(this.c_email, this.c_password).subscribe(
      data =>{
        this.res = data;
        if (this.res.status == "true"){
          this.router.navigate(['home']);
        }else{
          console.log("Wrong Password");
          this.router.navigate(['login']);
        }
      }
    )
  }

  public forgotPwd() {
    this.router.navigate(['frgpwd']);
  }




}

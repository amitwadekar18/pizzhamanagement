import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Resp } from 'src/app/entites/resp';
import { LoginServiceService } from 'src/app/service/login-service.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  c_email: any;
  res: Resp | undefined;

  constructor(private lservice: LoginServiceService,
    private router: Router) { }

  ngOnInit(): void {
  }

  public submitEmail() {
    this.lservice.forgotPassword(this.c_email).subscribe(
      data =>{
        this.res = data;
        if (this.res.status == "true"){
          console.log("Already registered!!");
          //this.router.navigate(['reset']);
          this.router.navigate(["reset", this.c_email]);
        }else{
          console.log("You are not registered");
        }
      }
    )
  }

}

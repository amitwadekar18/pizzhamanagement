import { Component, OnInit } from '@angular/core';
import { Resp } from 'src/app/entites/resp';
import { LoginServiceService } from 'src/app/service/login-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  r_email: any;
  r_password: any;
  r_c_password: any;
  r_name: any;
  r_p_number: any;
  r_address: any;
  res: Resp | undefined;

  constructor(private lservice: LoginServiceService) { }

  ngOnInit(): void {
  }

  registerNewCustomer(){
    this.lservice.insertUser(this.r_email, this.r_name, this.r_password, this.r_p_number, this.r_address).subscribe(
      data =>{
        this.res = data;
      }
    )

  }

}

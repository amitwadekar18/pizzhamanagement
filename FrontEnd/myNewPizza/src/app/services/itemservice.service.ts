import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Items } from '../entites/items';

@Injectable({
  providedIn: 'root'
})
export class ItemserviceService {

  private baseUrl = "http://localhost:8080/PizzhaMiniProject";

  
  constructor(private httpClient: HttpClient) { }

  getItemList(): Observable<Items[]>{
    console.log("getItemList");
    var mainUrl = this.baseUrl + "/itemmenu" ;
    return this.httpClient.get<GetResponse>(mainUrl).pipe(
      map( response => response.body)
    );
  }

  getCategoriesList(): Observable<String[]>{
    console.log("getCategoriesList");
    var mainUrl = this.baseUrl + "/itemtype";
    return this.httpClient.get<GetCategory>(mainUrl).pipe(
      map( response => response.body)
    );
  }

  getSubCategoryList(iCat: String): Observable<String[]>{
    console.log("getSubCategoryList");
    var mainUrl = this.baseUrl + "/itemsubtype";
    var inputBody = {
      itype: iCat
    }
    return this.httpClient.post<GetCategory>(mainUrl,inputBody).pipe(
      map( response => response.body)
    );
  }

  getPizzaList(iCat: String,iSubCat: String): Observable<Items[]>{
    console.log("getSubCategoryList");
    var mainUrl = this.baseUrl + "/getpizza";
    var inputBody = {
      itype: iCat,
      icategory: iSubCat
    }
    return this.httpClient.post<GetResponse>(mainUrl,inputBody).pipe(
      map( response => response.body)
    );
  }
}


interface GetResponse{
    body: Items[];
}

interface GetCategory{
  body: String[];
}

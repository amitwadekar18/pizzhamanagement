import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginServiceService } from './service/login-service.service';
import { LoginComponent } from './user/login/login.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'myNewPizza';

  constructor(private loginStat: LoginServiceService){
  }

  isUserAuthenticated$: Observable<boolean> | undefined;

  ngOnInit(): void {
    debugger;
    this.isUserAuthenticated$ = this.loginStat.IsLoggedIn;
  }

  

}

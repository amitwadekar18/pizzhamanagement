import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Resp } from '../entites/resp';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  private loginStatus = new BehaviorSubject<boolean>(this.checkLoginStatus());
  url: string = "http://localhost:8080/PizzhaMiniProject";
  constructor(private httpClient: HttpClient) { }
  result: Observable<GetLogin> | undefined;
  loginMethod(c_email: string, c_password: string): Observable<GetLogin>{
    const options = {
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    }
    var body = {
      cemail: c_email,
      cpassword: c_password
    };

    this.result = this.httpClient.post<GetLogin>(this.url + '/login', body, options).pipe(
      map(response => response)
    );
    //TODO:
    this.loginStatus.next(true);
    return this.result;

  }

  public insertUser(r_email: any, r_name: any, r_password: any, r_p_number: any, r_address: any): Observable<GetLogin> {
    const options = {
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    }
    var body = {
      cemail: r_email,
      cname: r_name,
      cpassword: r_password,
      cmobileno: r_p_number,
      caddress: r_address
    };
    return this.httpClient.post<GetLogin>(this.url + '/newCustomer', body, options);
  }

  public forgotPassword(r_email: any): Observable<GetLogin> {
    const options = {
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    }
    var body = {
      cemail: r_email
    };
    return this.httpClient.post<GetLogin>(this.url + '/getCustomerByMail', body, options);
  }

  public resetPassword(r_email: any,r_password: any): Observable<GetLogin> {
    console.log("email:",r_email);
    const options = {
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    }
    var body = {
      cemail: r_email,
      cpassword: r_password
    };
    return this.httpClient.post<GetLogin>(this.url + '/resetPwd', body, options);
  }

  checkLoginStatus(): boolean{
    return false;
  }

  get IsLoggedIn(){
    return this.loginStatus.asObservable();
  }

}
interface GetLogin {
  body: object;
  status: string;
}

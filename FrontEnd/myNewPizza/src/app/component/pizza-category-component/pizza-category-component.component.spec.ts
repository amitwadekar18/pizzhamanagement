import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaCategoryComponentComponent } from './pizza-category-component.component';

describe('PizzaCategoryComponentComponent', () => {
  let component: PizzaCategoryComponentComponent;
  let fixture: ComponentFixture<PizzaCategoryComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizzaCategoryComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaCategoryComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

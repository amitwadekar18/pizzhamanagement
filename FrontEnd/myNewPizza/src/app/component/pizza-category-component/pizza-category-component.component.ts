import { Component, OnInit } from '@angular/core';
import { Items } from 'src/app/entites/items';
import { ItemserviceService } from 'src/app/services/itemservice.service';

@Component({
  selector: 'app-pizza-category-component',
  templateUrl: './pizza-category-component.component.html',
  styleUrls: ['./pizza-category-component.component.css']
})
export class PizzaCategoryComponentComponent implements OnInit {

  constructor(private iService: ItemserviceService) { }

  pCategories: String[] | undefined;
  pSubCategories: String[] | undefined;
  iItems: Items[] | undefined;
  c_category: any;
  c_subcateogry: any;

  ngOnInit(): void {
    console.log("initcalled");
    this.listCategories();
  }
  listCategories() {
    this.iService.getCategoriesList().subscribe(
      data => {
        this.pCategories = data;
      }
    )
  }

  getSubCategory(event: any) {
    console.log(event.target.innerHTML);
    this.c_category = event.target.innerHTML;
    this.iService.getSubCategoryList(event.target.innerHTML).subscribe(
      data => {
        this.pSubCategories = data;
      }
    )
  }

  getList(event: any) {
    console.log(event.target.innerHTML);
    console.log(this.c_category);
    console.log(this.c_subcateogry);
    this.c_subcateogry = event.target.innerHTML;
    this.iService.getPizzaList(this.c_category, this.c_subcateogry).subscribe(
      data => {
        this.iItems = data;
      }
    )
  }

}

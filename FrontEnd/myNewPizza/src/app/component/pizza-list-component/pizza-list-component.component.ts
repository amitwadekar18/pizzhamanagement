import { Component, OnInit, Input } from '@angular/core';
import { Items } from 'src/app/entites/items';
import { ItemserviceService } from 'src/app/services/itemservice.service';

@Component({
  selector: 'app-pizza-list-component',
  templateUrl: './pizza-list-component.component.html',
  styleUrls: ['./pizza-list-component.component.css']
})
export class PizzaListComponentComponent implements OnInit {

  constructor(private iService: ItemserviceService) { }

  @Input() iItems: Items[] | undefined;
  ngOnInit(): void {
    console.log("initcalled");
    // this.listItems();
  }
}
